# Magento2 Parallax Widget #

Magento 2 Parallax Widget

## Features ##
* Well organized, easy to use and professionally designed.
* Uses the Magento 2 core Cms widget/blocks/pages functionality. 
* Ability to add multiple parallax widgets to the one page
* Built on the Simple Parallax Scrolling library with all options configurable
* Comes installed with a demo that can be viewed at {Magento2 Base Url}/parallax-widget/
* Can be installed without demo simply by removing the Setup directory.

## Install Magento 2 Parallax Widget ##
### Manual Installation ###

Install Parallax Widget

* Download the extension
* Unzip the file
* Create a folder {Magento root}/app/code/Oliverbode/ParallaxWidget
* Copy the content from the unzip folder
* Optionally remove Setup folder if you don't want to install the demo

### Composer Installation ###

```
#!

composer config repositories.oliverbode_parallaxwidget vcs https://oliver_bode@bitbucket.org/oliver_bode/oliverbode_parallaxwidget.git
composer require oliverbode/parallaxwidget
```

## Enable Parallax Widget ##

```
#!

php -f bin/magento module:enable --clear-static-content Oliverbode_ParallaxWidget
php -f bin/magento setup:upgrade
php -f bin/magento setup:static-content:deploy
```

## Creating a Parallax Widget ##

Log into your Magetno Admin, then goto Content -> Blocks

Create a block and upload image into content area. This will be the Parallax image

### Create on a single page ###

Goto Content -> Pages and either create or open an existing page.

In the content area select "Hide Editor" and Insert Widget. Choose the "Parallax Widget" fill in the widget options select the block you previously created and insert the widget.

### Create on one or more pages ###

Goto Content -> Widget and create a new widget. Choose the "Parallax Widget"

Select the pages where you wish to insert the widget and the block where you would like it to display. In the "Widget Options" select the blocks you previously created, optionally add the widget options and save widget.


### Removing Demo Parallax Widget ###

The demo can be viewed at: {Magento2 Base Url}/parallax-widget/

To remove it: log into your Magetno Admin, then goto Content -> Pages

Delete the "Parallax Widget".

Goto Content -> Blocks and delete the three blocks: Parallax Image (1,2 & 3)